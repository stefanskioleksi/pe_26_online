class Employee {
	constructor(name, age, salary) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	get name() {
		return this.name;
	}
	set name(value) {
		this._name = value;
	}

	get age() {
		return this.age;
	}
	set age(value) {
		this._age = value;
	}

	get salary() {
		return this.salary;
	}
	set salary(value) {
		this._salary = value;

	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this.lang = lang;
	}
	
	get salary() {
		return this.salary;
	}
	set salary(value) {
		this._salary = value * 3;
	}

}
const programmer1 = new Programmer('John', 30, 1000, ['JavaScript', 'Python']);
const programmer2 = new Programmer('Alice', 25, 6000, ['Java', 'C++']);

console.log(programmer1);
console.log(programmer2);
