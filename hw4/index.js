document.addEventListener('DOMContentLoaded', function () {
	const filmsListElement = document.getElementById('films-list');

	// Отримання списку фільмів
	fetch('https://swapi.dev/api/films/')
		.then(response => {
			if (!response.ok) {
				throw new Error('Failed to fetch films');
			}
			return response.json();
		})
		.then(data => {
			const films = data.results;
			films.forEach(film => {
				// Відображення інформації про фільм
				const filmElement = createFilmElement(film);
				filmsListElement.appendChild(filmElement);

				// Отримання та відображення списку персонажів фільму
				const charactersListElement = filmElement.querySelector('.characters-list');
				const loaderContainer = filmElement.querySelector('.loader-container');
				loaderContainer.style.display = 'block'; // Показати анімацію завантаження
				fetchCharacters(film.characters, charactersListElement, loaderContainer);
			});
		})
		.catch(error => {
			console.error('Error fetching films:', error);
			filmsListElement.innerHTML = '<p>Failed to fetch films. Please try again later.</p>';
		});

	// Функція для створення DOM-елементу фільму
	function createFilmElement(film) {
		const filmElement = document.createElement('div');
		filmElement.classList.add('film');
		filmElement.innerHTML = `
			<h2>Episode ${film.episode_id}: ${film.title}</h2>
			<p>${film.opening_crawl}</p>
			<div class="characters-list"></div>
			<div class="loader-container"><div class="loader"></div></div>
		`;
		return filmElement;
	}

	// Функція для отримання та відображення списку персонажів
	function fetchCharacters(characterUrls, charactersListElement, loaderContainer) {
		Promise.all(characterUrls.map(url => fetch(url).then(response => {
			if (!response.ok) {
				throw new Error('Failed to fetch characters');
			}
			return response.json();
		})))
			.then(characters => {
				const charactersHTML = characters.map(character => `<p>${character.name}</p>`).join('');
				charactersListElement.innerHTML = charactersHTML;
				loaderContainer.style.display = 'none'; 
			})
			.catch(error => {
				console.error('Error fetching characters:', error);
				charactersListElement.innerHTML = '<p>Failed to fetch characters. Please try again later.</p>';
				loaderContainer.style.display = 'none'; 
			});
	}
});
