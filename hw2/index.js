const books = [
	{
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70
	},
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	},
	{
		name: "Дизайн. Книга для недизайнерів.",
		price: 70
	},
	{
		author: "Алан Мур",
		name: "Неономікон",
		price: 70
	},
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	}
];

class BookList {
	constructor(books) {
		this.books = books.filter(book => this.isValidBook(book));
	}

	isValidBook(book) {
		try {
			if (!book || !book.author || !book.name || !book.price) {
				throw new Error(`Invalid book object: ${JSON.stringify(book)}. Missing property: ${!book ? 'whole object' : !book.author ? 'author' : !book.name ? 'name' : 'price'}`);
			}
			return true;
		} catch (error) {
			console.error(error.message);
			return false;
		}
	}

	render() {
		const root = document.getElementById('root');
		const ul = document.createElement('ul');

		this.books.forEach(book => {
			const li = document.createElement('li');
			li.classList.add('book-item');
			li.innerHTML = `
				<div class="book-details">
				<span class="name">${book.name}</span> 
				<span class="author">by ${book.author}</span>
				</div>
				<div class="book-price">
				<span class="price">$${book.price}</span>
				</div>
			`;
			ul.appendChild(li);
		});

		root.appendChild(ul);
	}
}

const bookList = new BookList(books);
bookList.render();


