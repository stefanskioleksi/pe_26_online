document.addEventListener('DOMContentLoaded', () => {
	const findIpBtn = document.getElementById('find-ip-btn');
	const ipInfoDiv = document.getElementById('ip-info');
	const continentSpan = document.getElementById('continent');
	const countrySpan = document.getElementById('country');
	const regionSpan = document.getElementById('region');
	const citySpan = document.getElementById('city');
	const districtSpan = document.getElementById('district');

	findIpBtn.addEventListener('click', async () => {
		try {
			const ipAddress = await getIpAddress();
			const ipDetails = await getIpDetails(ipAddress);
			updateIpInfo(ipDetails);
			ipInfoDiv.classList.remove('hidden');
		} catch (error) {
			console.error('Error:', error);
		}
	});

	async function getIpAddress() {
		const response = await fetch('https://api.ipify.org/?format=json');
		const data = await response.json();
		return data.ip;
	}

	async function getIpDetails(ipAddress) {
		const response = await fetch(`https://ip-api.com/json/${ipAddress}`);
		const data = await response.json();
		return {
			continent: data.continent,
			country: data.country,
			region: data.regionName,
			city: data.city,
			district: data.district
		};
	}

	function updateIpInfo(ipDetails) {
		continentSpan.textContent = ipDetails.continent;
		countrySpan.textContent = ipDetails.country;
		regionSpan.textContent = ipDetails.region;
		citySpan.textContent = ipDetails.city;
		districtSpan.textContent = ipDetails.district;
	}
});
