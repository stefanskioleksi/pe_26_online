document.addEventListener('DOMContentLoaded', function () {
	const loadingAnimation = document.getElementById('loadingAnimation');
	const postsContainer = document.getElementById('postsContainer');
	const addPostButton = document.getElementById('addPostButton');
	const modal = document.getElementById('modal');

	const closeButton = document.getElementsByClassName('close')[0];
	const submitPostButton = document.getElementById('submitPostButton');
	const postTitleInput = document.getElementById('postTitle');
	const postTextInput = document.getElementById('postText');

	function fetchPosts() {
		return fetch('https://ajax.test-danit.com/api/json/posts')
			.then(response => response.json());
	}

	function showLoadingAnimation() {
		loadingAnimation.style.display = 'block';
	}

	function hideLoadingAnimation() {
		loadingAnimation.style.display = 'none';
	}

	function displayPosts(posts) {
		postsContainer.innerHTML = '';
		posts.forEach(post => {
			const card = new Card(post);
			card.fetchUser().then(() => {
				postsContainer.appendChild(card.render());
			});
		});
	}

	function handleAddPost() {
		modal.style.display = 'block';
	}

	function closeModal() {
		modal.style.display = 'none';
	}

	addPostButton.addEventListener('click', handleAddPost);
	closeButton.addEventListener('click', closeModal);

	submitPostButton.addEventListener('click', function () {
		const newPost = {
			title: postTitleInput.value,
			body: postTextInput.value,
			userId: 1
		};

		fetch('https://ajax.test-danit.com/api/json/posts', {
			method: 'POST',
			body: JSON.stringify(newPost),
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then(response => response.json())
			.then(post => {
				const card = new Card(post);
				postsContainer.insertBefore(card.render(), postsContainer.firstChild);
				closeModal();
			});
	});

	showLoadingAnimation();
	fetchPosts()
		.then(posts => {
			hideLoadingAnimation();
			displayPosts(posts);
		})
		.catch(error => {
			console.error('Error fetching posts:', error);
			hideLoadingAnimation();
		});
	modal.addEventListener('click', function (event) {
		if (event.target === modal) {
			closeModal();
		}
	});
});

class Card {
	constructor(post) {
		this.post = post;
		this.user = null;
		this.cardElement = this.render();
	}

	fetchUser() {
		return fetch(`https://ajax.test-danit.com/api/json/users/${this.post.userId}`)
			.then(response => response.json())
			.then(user => {
				this.user = user;
			});
	}

	render() {
		const cardWrapper = document.createElement('div');
		cardWrapper.classList.add('card');
		cardWrapper.id = `post-${this.post.id}`;

		const title = document.createElement('h2');
		title.textContent = this.post.title;

		const body = document.createElement('p');
		body.textContent = this.post.body;

		const authorInfo = document.createElement('div');
		if (this.user) {
			authorInfo.textContent = `Author: ${this.user.name} (${this.user.email})`;
		} else {
			authorInfo.textContent = `Author: Loading...`;
		}

		const editButton = document.createElement('button');
		editButton.textContent = 'Edit';
		editButton.addEventListener('click', () => this.handleEdit());

		const deleteButton = document.createElement('button');
		deleteButton.textContent = 'Delete';
		deleteButton.addEventListener('click', () => this.deletePost());

		cardWrapper.appendChild(title);
		cardWrapper.appendChild(body);
		cardWrapper.appendChild(authorInfo);
		cardWrapper.appendChild(editButton);
		cardWrapper.appendChild(deleteButton);

		return cardWrapper;
	}

	handleEdit() {
		const editModal = document.createElement('div');
		editModal.classList.add('modal');
		editModal.innerHTML = `
			<div class="modal-content">
				<span class="close">&times;</span>
				<h2>Edit Post</h2>
				<input type="text" id="editTitle" value="${this.post.title}">
				<textarea id="editBody">${this.post.body}</textarea>
				<button id="saveEditButton">Save</button>
			</div>
		`;

		document.body.appendChild(editModal);
		editModal.style.display = 'block';

		const closeModal = () => {
			editModal.remove();
		};

		const saveEditButton = editModal.querySelector('#saveEditButton');
		saveEditButton.addEventListener('click', () => {
			const editedTitle = document.getElementById('editTitle').value;
			const editedBody = document.getElementById('editBody').value;
			this.saveEditedPost(this.post.id, editedTitle, editedBody);
			closeModal();
		});

		const closeButton = editModal.querySelector('.close');
		closeButton.addEventListener('click', closeModal);

		editModal.addEventListener('click', (event) => {
			if (event.target === editModal) {
				closeModal();
			}
		});
	}

	saveEditedPost(postId, editedTitle, editedBody) {
		const updatedPost = {
			title: editedTitle,
			body: editedBody,
			userId: this.post.userId
		};

		if (!document.getElementById(`post-${postId}`)) {
			console.error('Post not found');
			return;
		}

		fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
			method: 'PUT',
			body: JSON.stringify(updatedPost),
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then(response => {
				if (!response.ok) {
					throw new Error('Post not found or other error');
				}
				return response.json();
			})
			.then(updatedPost => {
				this.post = updatedPost;
				this.cardElement.querySelector('h2').textContent = updatedPost.title;
				this.cardElement.querySelector('p').textContent = updatedPost.body;

				const containerCard = document.getElementById(`post-${this.post.id}`);
				containerCard.querySelector('h2').textContent = updatedPost.title;
				containerCard.querySelector('p').textContent = updatedPost.body;
			})
			.catch(error => console.error('Error saving edited post:', error));
	}


	deletePost() {
		fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
			method: 'DELETE'
		})
			.then(() => {
				const element = document.getElementById(`post-${this.post.id}`);
				if (element) {
					element.remove();
				} else {
					console.error('Element not found');
				}
			})
			.catch(error => {
				console.error('Error deleting post:', error);
			});
	}
}
